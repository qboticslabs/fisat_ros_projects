//Author: Lentin Joseph
//Mail id: qboticslabs@gmail.com

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose.h"


int main(int argc, char **argv)
{
  ros::init(argc, argv, "move_turtle");

  
  //Inputting linear and angular velocity of the robot
  float lin_vel = atof(argv[1]);
  float ang_vel = atof(argv[2]);


  ROS_INFO("Starting Move Turtle Node");

  //Creating node handler and velocity publiser
  ros::NodeHandle n;

  ros::Publisher velocity_pub = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1000);


  ros::Rate loop_rate(20);

  //Sending velocity commands
  while (ros::ok())
  {
    geometry_msgs::Twist vel;

    vel.linear.x = lin_vel;
    vel.linear.y = 0;
    vel.linear.z = 0;

    vel.angular.x = 0;
    vel.angular.y = 0;
    vel.angular.z = ang_vel;

    velocity_pub.publish(vel);
 
    ROS_INFO("Sending Velocity command to robot");
    ROS_INFO("Linear Vel = %f: Angular Vel = %f",lin_vel,ang_vel);
    ros::spinOnce();

    loop_rate.sleep();


  }


  return 0;
}


