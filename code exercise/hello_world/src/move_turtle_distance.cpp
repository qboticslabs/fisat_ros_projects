//Author: Lentin Joseph
//Mail id: qboticslabs@gmail.com

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include "turtlesim/Pose.h"


float robot_x = 0;

void pose_callback(const turtlesim::Pose::ConstPtr& pose)
{

	ROS_INFO("Robot X = %f : Y=%f : Z=%f\n",pose->x,pose->y,pose->theta);

	robot_x = pose->x;


}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "move_turtle");

  
  //Inputting linear and angular velocity of the robot
  float lin_vel = atof(argv[1]);
  float ang_vel = atof(argv[2]);
  float distance = atof(argv[3]);

  ROS_INFO("Starting Move Turtle Node");

  //Creating node handler and velocity publiser
  ros::NodeHandle n;

  ros::Publisher velocity_pub = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1000);

  ros::Subscriber sub = n.subscribe("/turtle1/pose", 1000, pose_callback);

  ros::Rate loop_rate(20);

  //Sending velocity commands
  while (ros::ok())
  {
    geometry_msgs::Twist vel;

    vel.linear.x = lin_vel;
    vel.linear.y = 0;
    vel.linear.z = 0;

    vel.angular.x = 0;
    vel.angular.y = 0;
    vel.angular.z = ang_vel;

   if (robot_x >= distance){
	ROS_INFO("Robot Reached destination");
	ROS_INFO("Stopping robot");

	break;

    }

    velocity_pub.publish(vel);
 
    ROS_INFO("Sending Velocity command to robot");
    ROS_INFO("Linear Vel = %f: Angular Vel = %f",lin_vel,ang_vel);
    ros::spinOnce();

    loop_rate.sleep();


  }


  return 0;
}


